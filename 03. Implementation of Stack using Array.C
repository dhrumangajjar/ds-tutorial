#include<stdio.h>
#include<conio.h>

//Define Stack and initialize top with -1
int stack[10],top=-1;

//Function Declaration
void push();
void pop();
void peep();
void display();

//Execution of Main
void main()
{
	
	int choice,flag,count=0;
	clrscr();
	do
	{
	flag=0;
	clrscr();
	printf("Enter Number Accordingly:\n");
	printf("1. Push\n");
	printf("2. Pop\n");
	printf("3. Peep\n");
	printf("4. Traversal\n");
	printf("5. Exit:\n");
	printf("Enter Your Choice:");
	scanf("%d",&choice);
	switch (choice)
	{
		case 1:
		{
			push();
			flag=1;
			count++;
			break;

		}
		case 2:
		{
			pop();
			flag=1;
			count++;
			break;
		}
		case 3:
		{
			peep();
			flag=1;
			count++;
			break;
		}
		case 4:
		{
			display();
			flag=1;
			count++;
			break;
		}
		case 5:
		{
			flag=2;
			break;
		}
		default:
		{
			printf("Enter a Valid Choice");
		}
	}
	}
	while(flag==1);
	if(flag==2)
	{
		printf("\nYou have ran this loop for %d times\n",count);
		printf("\nYou have Exited the Program\n");

	}
	getch();
}

//Define "PUSH" that adds an element to stack
void push()
{

	//Check if Stack is Full
	if(top==9)
	{
		printf("\nThe Stack is Full\n");
		getch();
	}
	else
	{
		top++;
		printf("\nEnter Value on the Stack level %d\n:",top);
		scanf("%d",&stack[top]);
	}
}

//Define "POP" that Deletes Topmost Element from Stack
void pop()
{
	
	//Check if Stack is Empty
	if(top==-1)
	{
		printf("\nThe Stack is Empty\n");
		getch();
	}
	else
	{
		printf("\nDeleting the %d Stack level containing %d:\n",top+1,stack[top]);
		printf("\nStack level %d Deleted\n",top+1);
		top--;
		getch();
	}
}

//Define "PEEP" that displays topmost element
void peep()
{

	//Check if stack is empty
	if(top==-1)
	{
		printf("\nThe Stack is Empty\n");
	}
	else
	{
		printf("\nThe Value at Stack Level %d is %d\n",top+1,stack[top]);
		getch();
	}
}

//Define "DISPLAY" that traverses through the stack and displays each element
void display()
{
	int tmp;

	//Check if Stack is empty
	if(top==-1)
	{
		printf("\nStack is Empty\n");
	}
	else
	{
		printf("\nThe Stack Values from Level %d is:\n",top);
		tmp=top;
		while(tmp>=0)
		{
			printf("%d\n",stack[tmp]);
			tmp--;
		}
		getch();

	}
}
