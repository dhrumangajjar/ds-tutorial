#include<stdio.h>
#include<conio.h>
//Define Maximum Queue Length
#define MAX 10

//Initialize the Queue, Front and Rear as Global Variable
int queue[MAX],front=-1,rear=-1;
void insrt();
void del();
void disp();
void main()
{
	int n,flag;
	clrscr();
	do
	{
		flag=0;
		printf("\n\n1. Insert Element ");
		printf("\n2. Delete Element ");
		printf("\n3. Display Queue ");
		printf("\n4. Exit ");
		printf("\nEnter Choice : ");
		scanf("%d",&n);
		switch(n)
		{
			case 1  : insrt();
				  flag=1;
				  break;
			case 2  : del();
				  flag=1;
				  break;
			case 3  : disp();
				  flag=1;
				  break;
			case 4  : exit(1);
			default : printf("Enter valid choice.");
				  flag=1;
				  break;
		}
	}
	while(flag==1);
}

//Define Function For Insertion into the Queue
void insrt()
{
	int a;
	printf("\n\n     Enter number to insert : ");
	scanf("%d",&a);
	if(front==0 && rear==MAX-1)
	{
		printf("\n     Queue is overflow.");
	}
	else if(front==-1 && rear==-1)
	{
		front=0;
		rear=0;
		queue[rear]=a;
	}
	else if(front!=0 && rear==MAX-1)
	{
		rear=0;
		queue[rear]=a;
	}
	else
	{
		rear++;
		queue[rear]=a;
	}
}

//Define Deletion
void del()
{
	int x;
	if(front==-1)
	{
		printf("\n     Queue is underflow.");
	}
	x=queue[front];
	if(front==rear)
	{
		front=rear=-1;
	}
	else
	{
		if(front==MAX-1)
		{
			front=0;
		}
		else
		{
			front++;
		}
		printf("\n The deleted element is: %d",x);
	}
}

//Define the Display to Display the queue
void disp()
{
	int i;
	if(front==-1)
	{
		printf("\n No elements to display");
	}
	else
	{
		printf("\n The queue elements are:\n ");
		for(i=front;i<=rear;i++)
		{
			printf("\t %d",queue[i]);
		}
	}
}
